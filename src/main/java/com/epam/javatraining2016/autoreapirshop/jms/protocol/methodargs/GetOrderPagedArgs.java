package com.epam.javatraining2016.autoreapirshop.jms.protocol.methodargs;

public class GetOrderPagedArgs {
  public int clientId;
  public int fromOrder;
  public int pageLength;

  public GetOrderPagedArgs() {
  }

  public GetOrderPagedArgs(int clientId, int fromOrder, int pageLength) {
    this.clientId = clientId;
    this.fromOrder = fromOrder;
    this.pageLength = pageLength;
  }
}
