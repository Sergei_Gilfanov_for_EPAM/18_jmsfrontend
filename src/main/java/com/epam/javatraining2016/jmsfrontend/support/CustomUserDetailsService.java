package com.epam.javatraining2016.jmsfrontend.support;

import java.util.Arrays;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.epam.javatraining2016.autoreapirshop.api.AutoRepairShopService;
import com.epam.javatraining2016.autoreapirshop.jms.protocol.UserSearchResult;

@Service
public class CustomUserDetailsService implements UserDetailsService {
  private static final GrantedAuthority ROLE_REGISTERED_USER =
      new SimpleGrantedAuthority("ROLE_REGISTERED_USER");
  private static final GrantedAuthority ROLE_CHIEF_MANAGER =
      new SimpleGrantedAuthority("ROLE_CHIEF_MANAGER");

  private static final GrantedAuthority[] ROLES_REGISTERED_USER_CHIEF_MANAGER =
      {ROLE_REGISTERED_USER, ROLE_CHIEF_MANAGER};
  private static final GrantedAuthority[] ROLES_REGISTERED_USER = {ROLE_REGISTERED_USER};

  @Autowired
  public AutoRepairShopService backend;

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

    UserSearchResult userSearchResult = backend.getUser(username);

    if (!userSearchResult.isFound()) {
      throw new UsernameNotFoundException("Username not found");
    }

    Collection<GrantedAuthority> authorities;
    if (userSearchResult.getUser().isManager()) {
      authorities = Arrays.asList(ROLES_REGISTERED_USER_CHIEF_MANAGER);
    } else {
      authorities = Arrays.asList(ROLES_REGISTERED_USER);
    }

    CurrentUser retval = new CurrentUser(username, userSearchResult.getUser().getPassword(),
        authorities, userSearchResult.getUser().getFullName());

    return retval;
  }
}
