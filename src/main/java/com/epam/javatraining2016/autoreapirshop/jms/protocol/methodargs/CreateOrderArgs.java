package com.epam.javatraining2016.autoreapirshop.jms.protocol.methodargs;

public class CreateOrderArgs {
  public int clientId;
  public String commentText;

  public CreateOrderArgs() {}

  public CreateOrderArgs(int clientId, String commentText) {
    this.clientId = clientId;
    this.commentText = commentText;
  }

}
