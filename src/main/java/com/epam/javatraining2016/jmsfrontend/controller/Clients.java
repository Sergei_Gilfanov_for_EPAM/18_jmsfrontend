package com.epam.javatraining2016.jmsfrontend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.epam.javatraining2016.autoreapirshop.api.AutoRepairShopService;
import com.epam.javatraining2016.autoreapirshop.jms.protocol.Client;
import com.epam.javatraining2016.jmsfrontend.support.CurrentUser;

@Controller
@RequestMapping(value = "/clients")
public class Clients {
  @Autowired
  public AutoRepairShopService backend;

  @GetMapping
  public ModelAndView get(@AuthenticationPrincipal CurrentUser currentUser ) {
    ModelAndView mav = new ModelAndView();
    mav.addObject("currentUser", currentUser);
    // TODO: сделать тут и на бакенде List
    Client[] clients = backend.getClients();
    mav.addObject("clients", clients);
    mav.setViewName("clients");
    return mav;
  }
}
