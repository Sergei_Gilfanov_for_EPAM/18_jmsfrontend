package com.epam.javatraining2016.autoreapirshop.jms.protocol.methodargs;

public class CreateUserArgs {
  public String login;
  public String fullName;
  public String password;

  public CreateUserArgs() {}

  public CreateUserArgs(String login, String fullName, String password) {
    this.login = login;
    this.fullName = fullName;
    this.password = password;
  }

}
