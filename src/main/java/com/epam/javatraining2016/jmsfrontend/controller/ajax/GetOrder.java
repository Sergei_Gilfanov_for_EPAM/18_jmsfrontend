package com.epam.javatraining2016.jmsfrontend.controller.ajax;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.epam.javatraining2016.autoreapirshop.api.AutoRepairShopService;
import com.epam.javatraining2016.jmsfrontend.ajax.protocol.Order;

@RestController
@RequestMapping(value = "/backend/getOrder")
public class GetOrder {
  @Autowired
  public AutoRepairShopService backend;

  @RequestMapping(method = RequestMethod.GET)
  public Order get(@RequestParam int orderId) {
    com.epam.javatraining2016.autoreapirshop.jms.protocol.Order soapOrder = backend.getOrder(orderId);
    Order retval = new Order(soapOrder.getId(), soapOrder.getStatus());
    return retval;
  }
}
