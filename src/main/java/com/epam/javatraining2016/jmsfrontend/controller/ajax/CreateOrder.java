package com.epam.javatraining2016.jmsfrontend.controller.ajax;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.epam.javatraining2016.autoreapirshop.api.AutoRepairShopService;
import com.epam.javatraining2016.jmsfrontend.ajax.protocol.Id;

@RestController
@RequestMapping(value = "/backend/createOrder")
public class CreateOrder {
  @Autowired
  public AutoRepairShopService backend;

  @RequestMapping(method = RequestMethod.POST)
  public Id post(@RequestParam int clientId, @RequestParam String commentText) {
    int orderId = backend.createOrder(clientId, commentText);
    Id retval = new Id(orderId);
    return retval;
  }
}
