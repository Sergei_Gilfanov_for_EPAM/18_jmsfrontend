package com.epam.javatraining2016.jmsfrontend.controller.ajax;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.epam.javatraining2016.autoreapirshop.api.AutoRepairShopService;
import com.epam.javatraining2016.jmsfrontend.ajax.protocol.Client;

@RestController
@RequestMapping(value = "/backend/clientList")
public class ClientList {
  @Autowired
  public AutoRepairShopService backend;

  @RequestMapping(method = RequestMethod.GET)
  public List<Client> get() {
     com.epam.javatraining2016.autoreapirshop.jms.protocol.Client[] backendClients = backend.getClients();

    List<Client> retval = new ArrayList<Client>(backendClients.length);
    for (com.epam.javatraining2016.autoreapirshop.jms.protocol.Client backendClient : backendClients) {
      Client client = new Client(backendClient.getId(), backendClient.getClientName());
      retval.add(client);
    }
    return retval;
  }
}
