package com.epam.javatraining2016.autoreapirshop.jms.protocol;

import java.util.ArrayList;
import java.util.List;

public class OrdersPaged {

    protected Integer prevPageFrom;
    protected List<Order> order;
    protected Integer nextPageFrom;

    public Integer getPrevPageFrom() {
        return prevPageFrom;
    }

    public void setPrevPageFrom(Integer value) {
        this.prevPageFrom = value;
    }

    public List<Order> getOrder() {
        if (order == null) {
            order = new ArrayList<Order>();
        }
        return this.order;
    }

    public Integer getNextPageFrom() {
        return nextPageFrom;
    }

    public void setNextPageFrom(Integer value) {
        this.nextPageFrom = value;
    }

}
