package com.epam.javatraining2016.jmsfrontend.controller.ajax;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.epam.javatraining2016.autoreapirshop.api.AutoRepairShopService;
import com.epam.javatraining2016.jmsfrontend.ajax.protocol.Id;

@RestController
@RequestMapping(value = "/backend/changeStatus")
public class ChangeStatus {
  @Autowired
  public AutoRepairShopService backend;

  @RequestMapping(method = RequestMethod.POST)
  public Id post(@RequestParam int orderId, @RequestParam String status,
      @RequestParam String commentText) {
    int historyRecordId = backend.changeOrderStatus(orderId, status, commentText);
    Id retval = new Id(historyRecordId);
    return retval;
  }
}
