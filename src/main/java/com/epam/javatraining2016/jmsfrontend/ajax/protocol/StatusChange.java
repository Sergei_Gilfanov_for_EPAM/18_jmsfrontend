package com.epam.javatraining2016.jmsfrontend.ajax.protocol;

public class StatusChange extends Comment {
  public String status;

  public StatusChange(int id, String comment, String status) {
    super(id, comment);
    this.status = status;
  }
  
}
