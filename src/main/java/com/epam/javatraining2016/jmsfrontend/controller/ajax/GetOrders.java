package com.epam.javatraining2016.jmsfrontend.controller.ajax;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.epam.javatraining2016.autoreapirshop.api.AutoRepairShopService;
import com.epam.javatraining2016.jmsfrontend.ajax.protocol.Order;

@RestController
@RequestMapping(value = "/backend/getOrders")
public class GetOrders {
  @Autowired
  public AutoRepairShopService backend;

  @RequestMapping(method = RequestMethod.GET)
  public List<Order> get(@RequestParam int clientId) {
    com.epam.javatraining2016.autoreapirshop.jms.protocol.Order[] backendOrders = backend.getOrdersForClient(clientId);

    List<Order> retval = new ArrayList<Order>(backendOrders.length);

    for (com.epam.javatraining2016.autoreapirshop.jms.protocol.Order backendOrder : backendOrders) {
      Order order = new Order(backendOrder.getId(), backendOrder.getStatus());
      retval.add(order);
    }
    return retval;
  }
}
