package com.epam.javatraining2016.jmsfrontend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.epam.javatraining2016.autoreapirshop.api.AutoRepairShopService;
import com.epam.javatraining2016.jmsfrontend.support.PasswordsMismatchException;

@Controller
@RequestMapping(value = "/registration")
public class Registration {
  @Autowired
  public AutoRepairShopService backend;

  @GetMapping
  public ModelAndView get() {
    ModelAndView mav = new ModelAndView();
    mav.setViewName("registration");
    return mav;
  }

  @PostMapping
  public ModelAndView post(@RequestParam String login, @RequestParam String fullName,
      @RequestParam String password, @RequestParam String password2) {

    if (!password.equals(password2)) {
      throw new PasswordsMismatchException();
    }

    BCryptPasswordEncoder encoder = new BCryptPasswordEncoder(15);
    String encryptedPassword = encoder.encode(password2);
    backend.createUser(login, fullName, encryptedPassword);
    ModelAndView mav = new ModelAndView();
    mav.setViewName("redirect:/clients");
    return mav;
  }

}
