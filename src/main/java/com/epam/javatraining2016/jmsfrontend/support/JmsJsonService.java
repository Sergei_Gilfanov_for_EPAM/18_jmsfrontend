package com.epam.javatraining2016.jmsfrontend.support;

import javax.jms.JMSException;
import javax.jms.TextMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.jms.support.converter.MessageConversionException;
import org.springframework.stereotype.Service;

@Service
public class JmsJsonService {
  private static final Logger log = LoggerFactory.getLogger(JmsJsonService.class);

  @Autowired
  public JmsJsonMessageUtil jmsJsonMessageUtil;

  @Autowired
  public JmsTemplate jmsTemplate;

  JmsJsonService() {}

  public <RequestType, ResponseType> ResponseType sendAndReceive(String queue,
      RequestType unconvertedMessage) {
    MessageCreator messageCreator =
        jmsJsonMessageUtil.<RequestType>createMessageCreator(unconvertedMessage);

    // Конвертация должна быть безопасной - messageConverter с той стороны настроен так, что
    // он генерирует текстовые сообщения.
    TextMessage retMessage = (TextMessage) jmsTemplate.sendAndReceive(queue, messageCreator);
    try {
      log.debug("{}", retMessage.getText());
    } catch (JMSException e1) {
      throw new MessageConversionException("unchecked JMSException:", e1);
    }

    ResponseType retval = jmsJsonMessageUtil.<ResponseType>fromMessage(retMessage);
    return retval;
  }
}
