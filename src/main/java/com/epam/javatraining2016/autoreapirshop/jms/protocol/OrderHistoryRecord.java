package com.epam.javatraining2016.autoreapirshop.jms.protocol;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;

@JsonTypeInfo(
    use = JsonTypeInfo.Id.NAME,
    include = JsonTypeInfo.As.PROPERTY,
    property = "_type")
  @JsonSubTypes({
    @Type(value = OrderHistoryRecordComment.class, name = "comment"),
    @Type(value = OrderHistoryRecordStatusChange.class, name = "statusChange")
  })
public class OrderHistoryRecord {

    protected int id;

    public int getId() {
        return id;
    }

    public void setId(int value) {
        this.id = value;
    }

}
