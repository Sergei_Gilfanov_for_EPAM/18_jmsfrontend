package com.epam.javatraining2016.jmsfrontend.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.epam.javatraining2016.autoreapirshop.api.AutoRepairShopService;
import com.epam.javatraining2016.autoreapirshop.jms.protocol.Client;
import com.epam.javatraining2016.autoreapirshop.jms.protocol.Comment;
import com.epam.javatraining2016.autoreapirshop.jms.protocol.Order;
import com.epam.javatraining2016.autoreapirshop.jms.protocol.OrderHistoryRecord;
import com.epam.javatraining2016.autoreapirshop.jms.protocol.OrdersPaged;
import com.epam.javatraining2016.autoreapirshop.jms.protocol.UserSearchResult;
import com.epam.javatraining2016.autoreapirshop.jms.protocol.methodargs.ChangeOrderStatusArgs;
import com.epam.javatraining2016.autoreapirshop.jms.protocol.methodargs.CreateCommentArgs;
import com.epam.javatraining2016.autoreapirshop.jms.protocol.methodargs.CreateOrderArgs;
import com.epam.javatraining2016.autoreapirshop.jms.protocol.methodargs.CreateUserArgs;
import com.epam.javatraining2016.autoreapirshop.jms.protocol.methodargs.GetOrderPagedArgs;
import com.epam.javatraining2016.autoreapirshop.jms.protocol.methodargs.GetOrderPagedFilteredArgs;

@Service
public class AutoRepairShopServiceImpl implements AutoRepairShopService {
  @Autowired
  public JmsJsonService jmsJsonService;

  @Override
  public String getVersion() {
    String res = jmsJsonService.sendAndReceive("getVersion", "");
    return res;
  }

  @Override
  public int createOrder(int clientId, String commentText) {
    CreateOrderArgs arg = new CreateOrderArgs(clientId, commentText);
    Integer res = jmsJsonService.sendAndReceive("createOrder", arg);
    return res;
  }

  @Override
  public Order getOrder(int orderId) {
    Order res = jmsJsonService.sendAndReceive("getOrder", orderId);
    return res;
  }

  @Override
  public Order[] getOrdersForClient(int clientId) {
    Order[] res = jmsJsonService.sendAndReceive("getOrders", clientId);
    return res;
  }

  @Override
  public OrdersPaged getOrdersForClient(int clientId, int fromOrder, int pageLength,
      Integer statusId) {
    OrdersPaged res = null;
    if (statusId == null) {
      GetOrderPagedArgs arg = new GetOrderPagedArgs(clientId, fromOrder, pageLength);
      res = jmsJsonService.sendAndReceive("getOrdersPaged", arg);
    } else {
      GetOrderPagedFilteredArgs arg =
          new GetOrderPagedFilteredArgs(clientId, fromOrder, pageLength, statusId);
      res = jmsJsonService.sendAndReceive("getOrdersPagedFiltered", arg);
    }
    return res;
  }

  @Override
  public int changeOrderStatus(int orderId, String statusName, String commentText) {
    ChangeOrderStatusArgs arg = new ChangeOrderStatusArgs(orderId, statusName, commentText);
    Integer res = jmsJsonService.sendAndReceive("changeOrderStatus", arg);
    return res;
  }

  @Override
  public int createClient(String name) {
    Integer res = jmsJsonService.sendAndReceive("createClient", name);
    return res;
  }

  @Override
  public Client getClient(int clientId) {
    Client res = jmsJsonService.sendAndReceive("getClient", clientId);
    return res;
  }

  @Override
  public Client[] getClients() {
    Client[] res = jmsJsonService.sendAndReceive("getClients", "");
    return res;
  }

  @Override
  public int deleteClient(int clientId) {
    Integer res = jmsJsonService.sendAndReceive("deleteClient", clientId);
    return res;
  }

  @Override
  public int createComment(int orderId, String commentText) {
    CreateCommentArgs arg = new CreateCommentArgs(orderId, commentText);
    Integer res = jmsJsonService.sendAndReceive("createComment", arg);
    return res;
  }

  @Override
  public Comment getComment(int commentId) {
    Comment res = jmsJsonService.sendAndReceive("getComment", commentId);
    return res;
  }

  @Override
  public OrderHistoryRecord[] getHistory(int id) {
    OrderHistoryRecord[] res = jmsJsonService.sendAndReceive("getHistory", id);
    return res;
  }

  @Override
  public boolean loginAvailable(String loginName) {
    Boolean res = jmsJsonService.sendAndReceive("loginAvailable", loginName);
    return res;
  }

  @Override
  public int createUser(String login, String fullName, String password) {
    CreateUserArgs arg = new CreateUserArgs(login, fullName, password);
    Integer res = jmsJsonService.sendAndReceive("createUser", arg);
    return res;
  }

  @Override
  public UserSearchResult getUser(String loginName) {
    UserSearchResult res = jmsJsonService.sendAndReceive("getUser", loginName);
    return res;
  }
}
