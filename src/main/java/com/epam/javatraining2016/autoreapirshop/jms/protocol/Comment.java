package com.epam.javatraining2016.autoreapirshop.jms.protocol;

public class Comment {

  protected int id;
  protected String commentText;

  public int getId() {
      return id;
  }

  public void setId(int value) {
      this.id = value;
  }

  public String getCommentText() {
      return commentText;
  }

  public void setCommentText(String value) {
      this.commentText = value;
  }

}
