define(["jquery", "app/dao-0.1", "app/popups-0.1"], function ($, dao, popups) {
	var clientListPageElement = $('.client-list-page');
	var clientListElement = $('.client-list');

	clientListElement.find('.client-delete').click(function() {
		var client = {id: $(this).data().clientId};
		dao.client.delete(client)
		.done(function() {
			document.location.reload(true);
		});
	});

	clientListPageElement.find('.add-client-button').click(function() {
		popups.clientAdd.show()
		.done(function(newClient) {
			document.location.reload(true);
		});
	});
});
