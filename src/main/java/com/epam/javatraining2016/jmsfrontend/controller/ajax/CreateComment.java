package com.epam.javatraining2016.jmsfrontend.controller.ajax;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.epam.javatraining2016.autoreapirshop.api.AutoRepairShopService;
import com.epam.javatraining2016.jmsfrontend.ajax.protocol.Id;

@RestController
@RequestMapping(value = "/backend/createComment")
public class CreateComment {
  @Autowired
  public AutoRepairShopService backend;

  @RequestMapping(method = RequestMethod.POST)
  public Id post(@RequestParam int orderId, @RequestParam String commentText) {
    int commentId = backend.createComment(orderId, commentText);
    Id retval = new Id(commentId);
    return retval;
  }
}
