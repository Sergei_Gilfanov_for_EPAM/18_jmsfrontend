package com.epam.javatraining2016.jmsfrontend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.epam.javatraining2016.autoreapirshop.api.AutoRepairShopService;
import com.epam.javatraining2016.autoreapirshop.jms.protocol.OrdersPaged;
import com.epam.javatraining2016.jmsfrontend.support.CurrentUser;

@Controller
@RequestMapping(value = "/clients/{clientId}/orders")
public class Orders {
  @Autowired
  public AutoRepairShopService backend;

  @GetMapping
  public ModelAndView get(@AuthenticationPrincipal CurrentUser currentUser, @PathVariable int clientId, @RequestParam(required = false) Integer from,
      @RequestParam(required = false) Integer statusId) {
    final int pageLength = 5;
    ModelAndView mav = new ModelAndView();
    mav.addObject("currentUser", currentUser);
    mav.addObject("clientId", clientId);

    OrdersPaged ordersPaged;
    if (from == null) {
      ordersPaged = backend.getOrdersForClient(clientId, -1, pageLength, statusId);
    } else {
      ordersPaged = backend.getOrdersForClient(clientId, from, pageLength, statusId);
    }
    mav.addObject("ordersPaged", ordersPaged);

    if (statusId != null) {
      mav.addObject("statusId", String.format("statusId=%d&", statusId));
    }

    mav.setViewName("orders");
    return mav;
  }
}
