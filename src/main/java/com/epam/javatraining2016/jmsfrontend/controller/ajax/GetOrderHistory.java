package com.epam.javatraining2016.jmsfrontend.controller.ajax;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.epam.javatraining2016.autoreapirshop.api.AutoRepairShopService;
import com.epam.javatraining2016.autoreapirshop.jms.protocol.OrderHistoryRecord;
import com.epam.javatraining2016.autoreapirshop.jms.protocol.OrderHistoryRecordComment;
import com.epam.javatraining2016.autoreapirshop.jms.protocol.OrderHistoryRecordStatusChange;
import com.epam.javatraining2016.jmsfrontend.ajax.protocol.Comment;
import com.epam.javatraining2016.jmsfrontend.ajax.protocol.StatusChange;

@RestController
@RequestMapping(value = "/backend/getOrderHistory")
public class GetOrderHistory{
  @Autowired
  public AutoRepairShopService backend;

  @RequestMapping(method = RequestMethod.GET)
  public List<Comment> get(@RequestParam int orderId) {
    OrderHistoryRecord[] orderHistory = backend.getHistory(orderId);

    List<Comment> retval = new ArrayList<Comment>(orderHistory.length);
    for (OrderHistoryRecord historyRecord : orderHistory) {
      // Вся иерархия OrderHistoryRecord* - автогенерируемая из WSDL сервиса, поэтому
      // воспользоваться полиморфизмом пока не получилось. Будем проверять типы через
      // instanceof
      Comment toAdd = null;
      if (historyRecord instanceof OrderHistoryRecordStatusChange) {
        OrderHistoryRecordStatusChange orderHistoryRecordStatusChange =
            (OrderHistoryRecordStatusChange) historyRecord;
        StatusChange statusChange = new StatusChange(orderHistoryRecordStatusChange.getId(),
            orderHistoryRecordStatusChange.getComment().getCommentText(),
            orderHistoryRecordStatusChange.getStatus());
        toAdd = statusChange;
      } else if (historyRecord instanceof OrderHistoryRecordComment) {
        OrderHistoryRecordComment orderHistoryRecordComment =
            (OrderHistoryRecordComment) historyRecord;
        toAdd = new Comment(orderHistoryRecordComment.getId(),
            orderHistoryRecordComment.getComment().getCommentText());
      }
      retval.add(toAdd);
    }
    return retval;
  }
}
