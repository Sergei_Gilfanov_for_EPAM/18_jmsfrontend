package com.epam.javatraining2016.jmsfrontend.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


@Controller
@RequestMapping(value = "/login")
public class Login {

  @GetMapping
  public ModelAndView get() {
    ModelAndView mav = new ModelAndView();
    mav.setViewName("login");
    return mav;
  }

  @PostMapping
  public ModelAndView post(@RequestParam String username, @RequestParam String password) {
    ModelAndView mav = new ModelAndView();
    mav.setViewName("redirect:/clients");
    return mav;
  }
}
