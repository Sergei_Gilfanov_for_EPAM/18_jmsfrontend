package com.epam.javatraining2016.jmsfrontend.controller.ajax;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.epam.javatraining2016.autoreapirshop.api.AutoRepairShopService;
import com.epam.javatraining2016.jmsfrontend.ajax.protocol.Client;

@RestController
@RequestMapping(value = "/backend/createClient")
public class CreateClient {
  @Autowired
  public AutoRepairShopService backend;

  @RequestMapping(method = RequestMethod.POST)
  public Client post(@RequestParam String clientName) {
    int clientId = backend.createClient(clientName);
    Client retval = new Client(clientId, clientName);
    return retval;
  }
}
