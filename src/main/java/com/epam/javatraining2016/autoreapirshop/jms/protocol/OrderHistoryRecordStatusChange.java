package com.epam.javatraining2016.autoreapirshop.jms.protocol;

public class OrderHistoryRecordStatusChange
    extends OrderHistoryRecord
{
    protected String status;
    protected Comment comment;

    public String getStatus() {
        return status;
    }

    public void setStatus(String value) {
        this.status = value;
    }

    public Comment getComment() {
        return comment;
    }

    public void setComment(Comment value) {
        this.comment = value;
    }
}
