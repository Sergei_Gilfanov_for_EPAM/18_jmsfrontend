package com.epam.javatraining2016.jmsfrontend.controller.ajax;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.epam.javatraining2016.autoreapirshop.api.AutoRepairShopService;

@RestController
@RequestMapping(value = "/backend/deleteClient")
public class DeleteClient {
  @Autowired
  public AutoRepairShopService backend;

  @PostMapping
  public int post(@RequestParam int clientId) {
    return backend.deleteClient(clientId);
  }
}
