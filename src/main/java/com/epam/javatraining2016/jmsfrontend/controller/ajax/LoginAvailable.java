package com.epam.javatraining2016.jmsfrontend.controller.ajax;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.epam.javatraining2016.autoreapirshop.api.AutoRepairShopService;

@RestController
@RequestMapping(value = "/backend/loginAvailable")
public class LoginAvailable {
  @Autowired
  public AutoRepairShopService backend;

  @PostMapping
  public boolean get(@RequestParam String login, HttpServletResponse response) {
    boolean retval = backend.loginAvailable(login);
    if (retval) {
      response.setStatus(HttpServletResponse.SC_OK);
    } else {
      response.setStatus(HttpServletResponse.SC_CONFLICT);
    }
    return retval;
  }
}
