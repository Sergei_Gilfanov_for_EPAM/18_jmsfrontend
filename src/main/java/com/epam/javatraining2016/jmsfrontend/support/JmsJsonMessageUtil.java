package com.epam.javatraining2016.jmsfrontend.support;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.MessageCreator;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageConversionException;
import org.springframework.stereotype.Component;

@Component
public class JmsJsonMessageUtil {
  @Autowired
  public MappingJackson2MessageConverter jsonJmsMessageConverter;

  class JsonMessageCreator<T> implements MessageCreator {
    private T unconvertedMessage;

    public JsonMessageCreator(T unconvertedMessage) {
      this.unconvertedMessage = unconvertedMessage;
    }

    @Override
    public Message createMessage(Session session) throws JMSException {
      Message retval = jsonJmsMessageConverter.toMessage(unconvertedMessage, session);
      return retval;
    }
  }

  public <T> MessageCreator createMessageCreator(T unconvertedMessage) {
    JsonMessageCreator<T> retval = new JsonMessageCreator<T>(unconvertedMessage);
    return retval;
  }

  @SuppressWarnings("unchecked")
  public <T> T fromMessage(TextMessage jmsMessage) {
    T retval;
    try {
      retval = (T) jsonJmsMessageConverter.fromMessage(jmsMessage);
    } catch (JMSException e) {
      throw new MessageConversionException("unchecked JMSException:", e);
    }
    return retval;
  }
}
