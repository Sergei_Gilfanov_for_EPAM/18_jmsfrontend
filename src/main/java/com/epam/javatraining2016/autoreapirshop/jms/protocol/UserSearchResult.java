package com.epam.javatraining2016.autoreapirshop.jms.protocol;

public class UserSearchResult {

    protected boolean found;
    protected User user;

    public boolean isFound() {
        return found;
    }

    public void setFound(boolean value) {
        this.found = value;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User value) {
        this.user = value;
    }
}
